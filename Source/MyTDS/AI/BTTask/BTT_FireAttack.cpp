#include "BTT_FireAttack.h"

#include <AIController.h>

#include "AI_Character.h"

UBTT_FireAttack::UBTT_FireAttack()
{
	NodeName = "FireAttack";
}

EBTNodeResult::Type UBTT_FireAttack::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (AAI_Character* ControlledPawn = Cast<AAI_Character>(OwnerComp.GetAIOwner()->GetPawn()))
	{
		ControlledPawn->AttackPressed();
		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	}
	return Super::ExecuteTask(OwnerComp, NodeMemory);
}
