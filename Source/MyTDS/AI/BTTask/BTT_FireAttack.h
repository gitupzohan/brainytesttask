
#pragma once

#include <CoreMinimal.h>
#include <BehaviorTree/Tasks/BTTask_BlackboardBase.h>

#include "BTT_FireAttack.generated.h"


UCLASS()
class MYTDS_API UBTT_FireAttack : public UBTTask_BlackboardBase
{
	GENERATED_BODY()

	UBTT_FireAttack();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};


