﻿#include "AI_Character.h"

#include <Camera/CameraComponent.h>
#include <GameFramework/SpringArmComponent.h>

#include "MyTDSGameMode.h"

AAI_Character::AAI_Character()
{
	PrimaryActorTick.bCanEverTick = true;
	bIsAiming = true;
	bIsPlayer = false;
	GetTopDownCameraComponent()->DestroyComponent();
	GetCameraBoom()->DestroyComponent();
}

void AAI_Character::BeginPlay()
{
	Super::BeginPlay();
	AMyTDSGameMode* GameMode = GetWorld()->GetAuthGameMode<AMyTDSGameMode>();
	if(GameMode)
	{
		GameMode->AI_Character = this;
	}
}
