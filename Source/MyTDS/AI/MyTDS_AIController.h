// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Perception/AIPerceptionTypes.h"
#include "Perception/AISenseConfig.h"
#include "MyTDS_AIController.generated.h"

UCLASS()
class MYTDS_API AMyTDS_AIController : public AAIController
{
	GENERATED_BODY()

	AMyTDS_AIController();
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UAIPerceptionComponent* AIPerceptionComp;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category= "BT")
	UBehaviorTree* BTAsset;
	UPROPERTY()
	AActor* PerceivedActor;
	FTimerHandle LostSightTimer;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category= "Perception")
	TSubclassOf<UAISense> DominantSense;

	virtual void OnPossess(APawn* InPawn) override;
	void UpdateSightKey(bool HasLineOfSight);
	void UpdateTargetKey(AActor* Actor);
	void LostSight();
	UFUNCTION()
	void OnTargetPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus);
};
