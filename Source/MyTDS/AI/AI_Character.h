﻿
#pragma once

#include <CoreMinimal.h>

#include "MyTDSCharacter.h"
#include "AI_Character.generated.h"

UCLASS()
class MYTDS_API AAI_Character : public AMyTDSCharacter
{
	GENERATED_BODY()

public:
	
	AAI_Character();

protected:
	virtual void BeginPlay() override;
};
