#include "MyTDS_AIController.h"

#include <BehaviorTree/BlackboardComponent.h>
#include <Perception/AIPerceptionComponent.h>

AMyTDS_AIController::AMyTDS_AIController()
{
	PrimaryActorTick.bCanEverTick = true;
	AIPerceptionComp = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerception"));
	AIPerceptionComp->OnTargetPerceptionUpdated.AddDynamic(this, &AMyTDS_AIController::OnTargetPerceptionUpdated);
	AIPerceptionComp->SetDominantSense(DominantSense);
}

void AMyTDS_AIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	RunBehaviorTree(BTAsset);
}

void AMyTDS_AIController::UpdateSightKey(bool HasLineOfSight)
{
	if (GetBlackboardComponent())
		GetBlackboardComponent()->SetValueAsBool("HasLineOfSight", HasLineOfSight);
}

void AMyTDS_AIController::UpdateTargetKey(AActor* Actor)
{
	if (GetBlackboardComponent())
		GetBlackboardComponent()->SetValueAsObject("TargetActor", Actor);
}

void AMyTDS_AIController::LostSight()
{
	UpdateTargetKey(nullptr);
}

void AMyTDS_AIController::OnTargetPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus)
{
	PerceivedActor = Actor;
	if (Actor->ActorHasTag("Player"))
	{
		if (Stimulus.WasSuccessfullySensed())
		{
			GetWorldTimerManager().ClearTimer(LostSightTimer);
			UpdateSightKey(true);
			UpdateTargetKey(PerceivedActor);
		}
		else
		{
			GetWorldTimerManager().SetTimer(LostSightTimer, this, &ThisClass::LostSight, 8.f, false);
			UpdateSightKey(false);
		}
	}
}
