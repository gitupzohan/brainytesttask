
using UnrealBuildTool;

public class MyTDS : ModuleRules
{
	public MyTDS(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[]
		{
			"Core",
			"CoreUObject",
			"UMG",
			"Engine",
			"InputCore",
			"HeadMountedDisplay",
			"NavigationSystem",
			"AIModule",
			"GameplayTasks"
		});

		PublicIncludePaths.AddRange(new string[]
		{
			"MyTDS/Game",
			"MyTDS/PlayerPawn",
			"MyTDS/AI",
			"MyTDS/AI/BTTask",
			"MyTDS/Bullet"
		});
	}
}