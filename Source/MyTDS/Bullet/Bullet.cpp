﻿#include "Bullet.h"

#include <GameFramework/KillZVolume.h>

#include "MyTDSCharacter.h"


ABullet::ABullet()
{
	PrimaryActorTick.bCanEverTick = true;

	Sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
	Sphere->InitSphereRadius(3.f);
	Sphere->OnComponentHit.AddDynamic(this, &ABullet::SphereHit);
	Sphere->OnComponentBeginOverlap.AddDynamic(this, &ABullet::BallBeginOverlap);
	RootComponent = Sphere;
	
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);
	Mesh->SetCanEverAffectNavigation(false);
}

void ABullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	MovementBall(DeltaTime);
}

void ABullet::MovementBall(float DeltaSeconds)
{
	Sphere->AddWorldOffset(DeltaSeconds * Direction * Speed, true);
}

void ABullet::SphereHit(UPrimitiveComponent* PrimitiveComponent, AActor* Actor, UPrimitiveComponent* PrimitiveComponent1, FVector Vector, const FHitResult& HitResult)
{
	if (HitResult.bBlockingHit)
	{
		FVector ReflectionVector = FMath::GetReflectionVector(Direction, HitResult.Normal);
		Direction.Set(ReflectionVector.X, ReflectionVector.Y, 0);
		if(AMyTDSCharacter* HitActor = Cast<AMyTDSCharacter>(HitResult.GetActor()))
		{
			HitActor->SetLifeSpan(0.1);
			Destroy();
		}
	}
}

void ABullet::BallBeginOverlap(UPrimitiveComponent* PrimitiveComponent, AActor* Actor, UPrimitiveComponent* PrimitiveComponent1, int I, bool bArg, const FHitResult& HitResult)
{
	AKillZVolume* KillZVolume = Cast<AKillZVolume>(Actor);
	if (KillZVolume)
	{
		Destroy();
	}
}
