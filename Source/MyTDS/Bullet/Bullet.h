﻿#pragma once

#include <CoreMinimal.h>
#include <Components/SphereComponent.h>
#include <GameFramework/Actor.h>
#include "Bullet.generated.h"

UCLASS()
class MYTDS_API ABullet : public AActor
{
	GENERATED_BODY()

public:
	ABullet();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
	UStaticMeshComponent* Mesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sphere")
	USphereComponent* Sphere = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FVector Direction = FVector();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float Speed = 25.0f;

	void MovementBall(float DeltaSeconds);
	virtual void Tick(float DeltaTime) override;
	UFUNCTION()
	void SphereHit(UPrimitiveComponent* PrimitiveComponent, AActor* Actor, UPrimitiveComponent* PrimitiveComponent1,
	               FVector Vector, const FHitResult& HitResult);
	UFUNCTION()
	void BallBeginOverlap(UPrimitiveComponent* PrimitiveComponent, AActor* Actor,
	                      UPrimitiveComponent* PrimitiveComponent1, int I, bool bArg, const FHitResult& HitResult);
};
