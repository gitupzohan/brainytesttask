#include "MyTDSCharacter.h"

#include <DrawDebugHelpers.h>
#include <Blueprint/UserWidget.h>
#include <Camera/CameraComponent.h>
#include <Components/CapsuleComponent.h>
#include <GameFramework/CharacterMovementComponent.h>
#include <GameFramework/SpringArmComponent.h>
#include <Engine/World.h>
#include <Kismet/GameplayStatics.h>

#include "MyTDSGameMode.h"
#include "Bullet.h"

AMyTDSCharacter::AMyTDSCharacter()
{
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true);
	CameraBoom->TargetArmLength = 1200.f;
	CameraBoom->SetRelativeRotation(FRotator(-90.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false;

	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	StaticMesh->SetupAttachment(RootComponent);

	GetMesh()->DestroyComponent();

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void AMyTDSCharacter::BeginPlay()
{
	Super::BeginPlay();
	AMyTDSGameMode* GameMode = GetWorld()->GetAuthGameMode<AMyTDSGameMode>();
	if (GameMode && bIsPlayer)
	{
		GameMode->Player = this;
		GameMode->PlayerStartLocation = GetActorLocation();
	}
}

void AMyTDSCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	MovementTick(DeltaSeconds);
	RotationTick(DeltaSeconds);
	AimingTick();
}

void AMyTDSCharacter::MoveForward(float Value)
{
	AxisX = Value;
}

void AMyTDSCharacter::MoveRight(float Value)
{
	AxisY = Value;
}

void AMyTDSCharacter::RotateRight(float Value)
{
	AxisRotator = Value;
}

void AMyTDSCharacter::AttackPressed()
{
	if (CoolDownFire)
	{
		CoolDownFire = false;
		FTimerHandle Timer;
		GetWorldTimerManager().SetTimer(Timer, this, &ThisClass::FireTimer, RateFire, false);
		FVector Loc = GetCapsuleComponent()->GetComponentLocation() + GetCapsuleComponent()->GetForwardVector() * 50.f;
		ABullet* Bullet = GetWorld()->SpawnActor<ABullet>(BulletClass.LoadSynchronous(), Loc, FRotator(0.f));
		Bullet->Direction = GetCapsuleComponent()->GetForwardVector();
		UMaterialInstanceDynamic* DynamicMaterial = Bullet->Mesh->CreateDynamicMaterialInstance(
			0, Bullet->Mesh->GetMaterial(0));
		DynamicMaterial->SetVectorParameterValue("Color", BulletColor);
		OnAttack.Broadcast(RateFire, Timer);
	}
}

void AMyTDSCharacter::AimPressed()
{
	bIsAiming = true;
}

void AMyTDSCharacter::AimReleased()
{
	bIsAiming = false;
}

void AMyTDSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ThisClass::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ThisClass::MoveRight);

	PlayerInputComponent->BindAxis("RotateRight", this, &ThisClass::RotateRight);
	
	PlayerInputComponent->BindAction("AttackAction", IE_Pressed, this, &ThisClass::AttackPressed);

	PlayerInputComponent->BindAction("AimAction", IE_Pressed, this, &ThisClass::AimPressed);
	PlayerInputComponent->BindAction("AimAction", IE_Released, this, &ThisClass::AimReleased);
}

void AMyTDSCharacter::FireTimer()
{
	CoolDownFire = true;
	OnReloadEnd.Broadcast();
}

void AMyTDSCharacter::Destroyed()
{
	OnDestroyed.RemoveAll(this);
	Super::Destroyed();
}

void AMyTDSCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX, false);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY, false);
}

void AMyTDSCharacter::RotationTick(float DeltaTime)
{
	if (Controller)
	{
		Controller->SetControlRotation(GetCapsuleComponent()->GetComponentRotation() + FRotator(0.f, AxisRotator * DeltaTime * RotationRate, 0.f));
	}
}

void AMyTDSCharacter::AimingTick()
{
	if (bIsAiming)
	{
		FVector TraceStart = GetCapsuleComponent()->GetComponentLocation();
		FVector TraceEnd = TraceStart + GetCapsuleComponent()->GetForwardVector() * 3000.f;
		FHitResult HitResult;
		TraceCout = 0;
		TArray<AActor*> IgnoredActors;
		IgnoredActors.Add(this);
		UKismetSystemLibrary::SphereTraceSingle(GetWorld(), TraceStart, TraceEnd, RadiusSphereTrace,
		                                        ETraceTypeQuery::TraceTypeQuery1, false, IgnoredActors,
		                                        EDrawDebugTrace::None, HitResult, true);
		if (HitResult.bBlockingHit)
		{
			DrawDebugLine(GetWorld(), TraceStart, HitResult.ImpactPoint, TraceColor, false, 0.02f, 0, 2.f);
			if (Cast<AMyTDSCharacter>(HitResult.GetActor()))
			{
				CanFire = true;
				return;
			}
			else
			{
				CanFire = false;
				AimLine(HitResult);
				return;
			}
		}
		DrawDebugLine(GetWorld(), TraceStart, TraceEnd, TraceColor, false, 0.02f, 0, 2.f);
	}
}

void AMyTDSCharacter::AimLine(FHitResult HitResult)
{
	FVector TraceStart = HitResult.ImpactPoint;
	FVector Direction = HitResult.TraceEnd - HitResult.TraceStart;
	FVector TraceEnd = TraceStart + FMath::GetReflectionVector(Direction, HitResult.Normal);

	FHitResult HitResultSecondary;
	TArray<AActor*> IgnoredActors;
	IgnoredActors.Add(this);
	IgnoredActors.Add(HitResult.GetActor());

	UKismetSystemLibrary::SphereTraceSingle(GetWorld(), TraceStart, TraceEnd, RadiusSphereTrace,
	                                        ETraceTypeQuery::TraceTypeQuery1, false, IgnoredActors,
	                                        EDrawDebugTrace::None, HitResultSecondary, true);
	TraceCout++;
	if (HitResultSecondary.bBlockingHit)
	{
		DrawDebugLine(GetWorld(), TraceStart, HitResultSecondary.ImpactPoint, TraceColor, false, 0.02f, 0, 2.f);
		if (Cast<AMyTDSCharacter>(HitResult.GetActor()))
		{
			CanFire = true;
			return;
		}
		else
		{
			CanFire = false;
			if (TraceCout < MaxTraceCout)
			{
				AimLine(HitResultSecondary);
			}
			return;
		}
	}
	DrawDebugLine(GetWorld(), TraceStart, HitResultSecondary.TraceEnd, TraceColor, false, 0.02f, 0, 2.f);
}
