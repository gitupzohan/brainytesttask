#pragma once

#include <CoreMinimal.h>
#include <GameFramework/Character.h>

#include "MyTDSCharacter.generated.h"

class ABullet;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAttack, float, ReloadTime, FTimerHandle, Timer);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnReloadEnd);

UCLASS(Blueprintable)
class AMyTDSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AMyTDSCharacter();
	
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	FORCEINLINE class UStaticMeshComponent* GetStaticMesh() const { return StaticMesh; }

	UPROPERTY(BlueprintReadWrite)
	bool bIsAiming = false;

	UPROPERTY(BlueprintReadWrite)
	bool CanFire = false;

	bool CoolDownFire = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category= "Attack")
	float RateFire = 1.f;

	UPROPERTY(BlueprintAssignable)
	FOnAttack OnAttack;
	UPROPERTY(BlueprintAssignable)
	FOnReloadEnd OnReloadEnd;

	bool bIsPlayer = true;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* StaticMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(EditDefaultsOnly, Category= "Bullet")
	TSoftClassPtr<ABullet> BulletClass = nullptr;
	UPROPERTY(EditDefaultsOnly, Category= "Bullet")
	FLinearColor BulletColor;

	UPROPERTY(EditDefaultsOnly, Category= "Movement")
	float RotationRate = 100.f;

	float AxisY;
	float AxisX;
	float AxisRotator;

	int32 TraceCout = 0;
	UPROPERTY(EditDefaultsOnly, Category= "NumTrace")
	FColor TraceColor = FColor::Green;
	UPROPERTY(EditDefaultsOnly, Category= "NumTrace")
	int32 MaxTraceCout = 10;
	UPROPERTY(EditDefaultsOnly, Category= "NumTrace")
	float RadiusSphereTrace = 3.f;
protected:
	virtual void BeginPlay() override;
public:
	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable)
	void AttackPressed();

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	virtual void Destroyed() override;
private:
	void AimPressed();
	void AimReleased();
	void MoveForward(float Value);
	void MoveRight(float Value);
	void RotateRight(float Value);
	void MovementTick(float DeltaTime);
	void RotationTick(float DeltaTime);
	void AimingTick();
	void FireTimer();
	void AimLine(FHitResult HitResult);
};
