#include "MyTDSPlayerController.h"

#include <Blueprint/AIBlueprintHelperLibrary.h>
#include <Blueprint/UserWidget.h>


AMyTDSPlayerController::AMyTDSPlayerController()
{
	bShowMouseCursor = false;
}

void AMyTDSPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	if (!WidgetInfo)
	{
		WidgetInfo = CreateWidget(this, WidgetInfoClass.LoadSynchronous());
		if (WidgetInfo)
		{
			WidgetInfo->AddToViewport();
		}
	}
	OnPosses.Broadcast(InPawn);
}

void AMyTDSPlayerController::OnUnPossess()
{
	Super::OnUnPossess();
	OnUnPosses.Broadcast();
}
