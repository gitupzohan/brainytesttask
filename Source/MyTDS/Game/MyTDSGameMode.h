#pragma once

#include <CoreMinimal.h>
#include <GameFramework/GameModeBase.h>

#include "MyTDSGameMode.generated.h"

class AMyTDS_GameState;
class AMyTDSCharacter;
class ATargetPoint;
class AAI_Character;
UCLASS(minimalapi)
class AMyTDSGameMode : public AGameModeBase
{
	GENERATED_BODY()

	AMyTDSGameMode();

public:
	UPROPERTY(EditDefaultsOnly, Category="EnemyClass")
	TSubclassOf<AAI_Character> EnemyClass = nullptr;
	UPROPERTY(EditDefaultsOnly, Category="Score")
	int32 MaxScore = 3;
	UPROPERTY()
	AMyTDS_GameState* CurrentGameState = nullptr;
	UPROPERTY()
	FVector EnemyRestartPoint = FVector();
	UPROPERTY()
	FVector PlayerStartLocation = FVector();
	UPROPERTY()
	AAI_Character* AI_Character = nullptr;
	UPROPERTY()
	AMyTDSCharacter* Player = nullptr;

private:
	void BindDestroyPlayer();
	void BindDestroyEnemy();
protected:
	void ReturnPlayers(bool bIsPlayer);
	UFUNCTION()
	void RestartPlayerOnDestroy(AActor* Actor);
	UFUNCTION()
	void RespawnEnemyOnDestroy(AActor* Actor);
	virtual void BeginPlay() override;
};
