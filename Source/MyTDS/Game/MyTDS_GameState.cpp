#include "MyTDS_GameState.h"

AMyTDS_GameState::AMyTDS_GameState()
{
}

void AMyTDS_GameState::IncrementScore(int32 Player, int32 Enemy)
{
	PlayerScore += Player;
	EnemyScore += Enemy;
	OnUpdateScore.Broadcast(PlayerScore, EnemyScore);
}

void AMyTDS_GameState::EndGame(bool bIsWin)
{
	OnEndGame.Broadcast(bIsWin);
}
