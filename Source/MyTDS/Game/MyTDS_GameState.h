#pragma once

#include <CoreMinimal.h>
#include <GameFramework/GameStateBase.h>
#include "MyTDS_GameState.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateScore, int32, PlayerScore, int32, EnemyScore);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEndGame, bool, bIsWin);

UCLASS()
class MYTDS_API AMyTDS_GameState : public AGameStateBase
{
	GENERATED_BODY()
	AMyTDS_GameState();
public:
	UPROPERTY(BlueprintAssignable)
	FOnUpdateScore OnUpdateScore;
	UPROPERTY(BlueprintAssignable)
	FOnEndGame OnEndGame;
	
	int32 PlayerScore = 0;
	int32 EnemyScore = 0;
	void IncrementScore(int32 Player = 0, int32 Enemy = 0);
	void EndGame(bool bIsWin);
};
