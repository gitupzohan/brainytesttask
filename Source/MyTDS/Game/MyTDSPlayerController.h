
#pragma once

#include <CoreMinimal.h>
#include <GameFramework/PlayerController.h>
#include "MyTDSPlayerController.generated.h"

class UUserWidget;
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPosses,APawn*, Pawn);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnUnPosses);
UCLASS()
class AMyTDSPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AMyTDSPlayerController();
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="WidgetInfo")
	TSoftClassPtr<UUserWidget> WidgetInfoClass;
	UPROPERTY()
	UUserWidget* WidgetInfo = nullptr;
	UPROPERTY(BlueprintAssignable)
	FOnPosses OnPosses;
	UPROPERTY(BlueprintAssignable)
	FOnUnPosses OnUnPosses;
	
protected:
	virtual void OnPossess(APawn* InPawn) override;
	virtual void OnUnPossess() override;

};


