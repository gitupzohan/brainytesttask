#include "MyTDSGameMode.h"

#include <EngineUtils.h>
#include <Engine/TargetPoint.h>
#include <Kismet/GameplayStatics.h>

#include "MyTDSPlayerController.h"
#include "MyTDSCharacter.h"
#include "MyTDS_GameState.h"
#include "AI_Character.h"

AMyTDSGameMode::AMyTDSGameMode()
{
	PlayerControllerClass = AMyTDSPlayerController::StaticClass();
	DefaultPawnClass = AMyTDSCharacter::StaticClass();
}

void AMyTDSGameMode::BindDestroyPlayer()
{
	for (TActorIterator<AMyTDSCharacter> It(GetWorld()); It; ++It)
	{
		AMyTDSCharacter* PlayerChar = *It;
		if (PlayerChar && PlayerChar->ActorHasTag("Player"))
		{
			PlayerChar->OnDestroyed.AddDynamic(this, &AMyTDSGameMode::RestartPlayerOnDestroy);
		}
	}
}

void AMyTDSGameMode::BindDestroyEnemy()
{
	for (TActorIterator<AAI_Character> It(GetWorld()); It; ++It)
	{
		AAI_Character* EnemyChar = *It;
		if (EnemyChar)
		{
			EnemyChar->OnDestroyed.AddDynamic(this, &AMyTDSGameMode::RespawnEnemyOnDestroy);
		}
	}
}

void AMyTDSGameMode::ReturnPlayers(bool bIsPlayer)
{
	if (bIsPlayer)
	{
		if (AI_Character)
		{
			AI_Character->SetActorLocation(EnemyRestartPoint);
		}
	}
	else
	{
		if (Player)
		{
			Player->SetActorLocation(PlayerStartLocation);
		}
	}
}

void AMyTDSGameMode::BeginPlay()
{
	Super::BeginPlay();
	BindDestroyPlayer();
	BindDestroyEnemy();
	
	UKismetSystemLibrary::ExecuteConsoleCommand(GetWorld(), FString(TEXT("t.MaxFPS 60")));
	
	for (TActorIterator<ATargetPoint> It(GetWorld()); It; ++It)
	{
		ATargetPoint* TargetPoint = *It;
		if (TargetPoint)
		{
			EnemyRestartPoint = TargetPoint->GetActorLocation();
		}
	}
	CurrentGameState = GetGameState<AMyTDS_GameState>();
}

void AMyTDSGameMode::RestartPlayerOnDestroy(AActor* Actor)
{
	ReturnPlayers(true);
	if (Actor)
	{
		Actor->OnDestroyed.RemoveDynamic(this, &AMyTDSGameMode::RestartPlayerOnDestroy);
	}
	if (CurrentGameState && CurrentGameState->EnemyScore < MaxScore)
	{
		auto Controller = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (Controller)
		{
			RestartPlayerAtPlayerStart(Controller, FindPlayerStart(Controller, "Player"));
		}
		BindDestroyPlayer();
		CurrentGameState->IncrementScore(0, 1);
	}
	else if (CurrentGameState)
	{
		CurrentGameState->EndGame(false);
	}
}

void AMyTDSGameMode::RespawnEnemyOnDestroy(AActor* Actor)
{
	ReturnPlayers(false);
	if (Actor)
	{
		Actor->OnDestroyed.RemoveDynamic(this, &AMyTDSGameMode::RespawnEnemyOnDestroy);
	}
	if (CurrentGameState && CurrentGameState->PlayerScore < MaxScore)
	{
		FActorSpawnParameters SpawnParam;
		SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
		AAI_Character* Enemy = GetWorld()->SpawnActor<AAI_Character>(EnemyClass, FTransform(EnemyRestartPoint), SpawnParam);
		Enemy->OnDestroyed.AddDynamic(this, &AMyTDSGameMode::RespawnEnemyOnDestroy);
		CurrentGameState->IncrementScore(1, 0);
	}
	else if (CurrentGameState)
	{
		CurrentGameState->EndGame(true);
	}
}
